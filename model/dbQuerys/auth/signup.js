const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

function checkExsitUser(params,langid){
    return new Promise(function (resolve, reject) {


        var prepareSql = "SELECT `users_db`.* "+
            "FROM `users_db` "+
            "WHERE `users_db`.`phone`="+db.escape(params.phone);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkExsitUser",
                        filds:{params:params},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/auth.txt")
                reject(new Error("Sql error checkExsitUser"))
            }else{
                resolve(rows)
            }
        });
    })
}

function checkExsitUserByToken(token){
    return new Promise(function (resolve, reject) {


        var prepareSql = "SELECT `users_db`.`id` "+
            "FROM `users_db` "+
            "WHERE `users_db`.`token`="+db.escape(token);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkExsitUser",
                        filds:{token:token},
                        date:new Date()
                    }
                };
                log.insertLog(JSON.stringify(errorData),"./logs/auth.txt")
                reject(new Error("Sql error checkExsitUser"))
            }else{
                resolve(rows)
            }
        });
    })
}
function checkUserPhoneCode(code){
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT `phone_code`.`id`,`phone_code`.`user_id` "+
            "FROM `phone_code` "+
            "WHERE `phone_code`.`code`="+db.escape(code);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkUserPhoneCode",
                        filds:{params:params},
                        date:new Date()
                    }
                };
                log.insertLog(JSON.stringify(errorData),"./logs/auth.txt")
                reject(new Error("checkUserPhoneCode"))
            }else{
                resolve(rows)
            }
        });
    })
}

function checkUserPhoneNumber(phone){
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT `users_db`.`id` "+
            "FROM `users_db` "+
            "WHERE `users_db`.`phone`="+db.escape(phone);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkUserPhoneCode",
                        filds:{params:params},
                        date:new Date()
                    }
                };
                log.insertLog(JSON.stringify(errorData),"./logs/auth.txt")
                reject(new Error("checkUserPhoneCode"))
            }else{
                resolve(rows)
            }
        });
    })
}



function updateUserStatus(user_id) {
    return new Promise(function (resolve, reject) {
        var prepareSql = "UPDATE `users_db` SET `status`= 1 WHERE `id`=" + db.escape(user_id) + "";
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "checkUserPhoneCode",
                        filds: {user_id: user_id},
                        date: new Date()
                    }
                };
                log.insertLog(JSON.stringify(errorData), "./logs/auth.txt")
                reject(new Error("checkUserPhoneCode"))
            } else {
                resolve(rows)
            }
        })
    });
}
function getUserInFinish(user_id) {
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT  `users_db`.`f_name`, `users_db`.`l_name`, `users_db`.`email` , `users_db`.`phone` ,  `users_db`.`status` ,  `users_db`.`token` FROM `users_db` WHERE `id`=" + db.escape(user_id) + "";
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "getUserInFinish",
                        filds: {user_id: user_id},
                        date: new Date()
                    }
                };
                log.insertLog(JSON.stringify(errorData), "./logs/auth.txt")
                reject(new Error("checkUserPhoneCode"))
            } else {
                resolve(rows)
            }
        })
    });
}

function deletePhoneCodeInFinish(user_id) {
    return new Promise(function (resolve, reject) {
        var prepareSql = "DELETE FROM `phone_code` WHERE `user_id`=" + db.escape(user_id) + "";
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "getUserInFinish",
                        filds: {user_id: user_id},
                        date: new Date()
                    }
                };
                log.insertLog(JSON.stringify(errorData), "./logs/auth.txt")
                reject(new Error("checkUserPhoneCode"))
            } else {
                resolve(rows)
            }
        })
    });
}



module.exports.checkExsitUser = checkExsitUser;
module.exports.getUserInFinish = getUserInFinish;
module.exports.checkExsitUserByToken = checkExsitUserByToken;
module.exports.checkUserPhoneCode = checkUserPhoneCode;
module.exports.deletePhoneCodeInFinish = deletePhoneCodeInFinish;
module.exports.updateUserStatus = updateUserStatus;
module.exports.checkUserPhoneNumber = checkUserPhoneNumber;




