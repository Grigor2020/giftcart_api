const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var findAllCategories = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT * FROM categories ORDER BY `id` DESC';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllCategories",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllCategories"))
            }else{
                for(var i = 0; i < result.length; i++){
                    result[i].logo = static.MAIN_URL + "/images/logos/" + result[i]['logo'];
                }
                resolve(result)
            }
        })
    })
};

module.exports.findAllCategories = findAllCategories;