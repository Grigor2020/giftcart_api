const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var addToCart = function(datas){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO cart SET ?';
        db.query(prepareSql, datas, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"addToCart",
                        table:'cart',
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getCatalog"))
            }else{
                resolve(result)
            }
        })
    })
};

function getCartByUser(params){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `cart`.`product_id`, `cart`.`count` , " +
            " `products`.`cat_id`, `products`.`price`, `products`.`real_price`, `products`.`description` ," +
            " `categories`.`name`,`categories`.`name_pharsi`,`categories`.`logo`,`categories`.`color`" +
            " FROM `cart`" +
            " JOIN `products` ON `products`.`id` = `cart`.`product_id`" +
            " JOIN `categories` ON `categories`.`id` = `products`.`cat_id`" +
            " WHERE `cart`.`"+params.user+"`="+db.escape(params.user_id)+" " +
            " ORDER BY `cart`.`id` DESC";
        console.log(prepareSql);

        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getCartByUser",
                        filds:{params:params},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/cart.txt")
                reject(new Error("Sql error getCartByUser"))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        });
    })
}

function getCartByUserAfterInsert(params){
    return new Promise(function (resolve, reject) {
        var prepareSql = "" +
            " SELECT `cart`.`product_id`, `cart`.`count` , " +
            " `products`.`cat_id`, `products`.`price`, `products`.`real_price`, `products`.`description` ," +
            " `categories`.`name`,`categories`.`name_pharsi`,`categories`.`logo`,`categories`.`color`" +
            " FROM `cart`" +
            " JOIN `products` ON `products`.`id` = `cart`.`product_id`" +
            " JOIN `categories` ON `categories`.`id` = `products`.`cat_id`" +
            " WHERE `cart`.`"+params.user+"`="+db.escape(params.user_id) +" AND `cart`.`id` = "+db.escape(params.id);
        console.log(prepareSql);
        // return false;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getCartByUser",
                        filds:{params:params},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/cart.txt")
                reject(new Error("Sql error getCartByUser"))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        });
    })
}

function updateCartCount(params){
    return new Promise(function (resolve, reject) {
        var arrow = '';
        if(params.actionType == 'minus'){
            arrow = '-';
        }else{
            arrow = '+';
        }
        var prepareSql = "UPDATE `cart` SET `count` = `count` "+arrow+" 1 WHERE `product_id` = "+db.escape(params.product_id) + " AND `"+params.user+"`="+db.escape(params.user_id)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getCartByUser",
                        filds:{params:params},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/cart.txt")
                reject(new Error("Sql error getCartByUser"))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        });
    })
}

function deleteCart(params){
    return new Promise(function (resolve, reject) {
        var prepareSql = "DELETE FROM `cart` WHERE `product_id` = "+db.escape(params.product_id) + " AND `"+params.user+"`="+db.escape(params.user_id)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"deleteCart",
                        filds:{params:params},
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/cart.txt")
                reject(new Error("Sql error getCartByUser"))
            }else{
                resolve(rows)
            }
        });
    })
}

module.exports.addToCart = addToCart;
module.exports.getCartByUser = getCartByUser;
module.exports.getCartByUserAfterInsert = getCartByUserAfterInsert;
module.exports.updateCartCount = updateCartCount;
module.exports.deleteCart = deleteCart;
