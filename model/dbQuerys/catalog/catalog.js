const db = require('../../connection');
var log = require("../../../logs/log");
const static = require('../../../static');

var getFlipMoneys = function(cat_id,country_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `flipmoney_cards`.`price` as price,`flipmoney_cards`.`id`, `flipmoney_rate`.`rate`' +
            ' FROM `flipmoney_cards`' +
            ' JOIN `flipmoney_rate`';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getFlipMoneys",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getFlipMoneys"))
            }else{
                resolve(result)
            }
        })
    })
};

var getCatalogByCountries = function(cat_id,country_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT * FROM `products` WHERE `cat_id`=' + db.escape(cat_id) +' AND `country_id`='+ db.escape(country_id) +' AND `status_buy`=0';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getCatalogByCountries",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getCatalogByCountries"))
            }else{
                resolve(result)
            }
        })
    })
};

var getCatalog = function(cat_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `products`.`cat_id`,`products`.`id` as prod_id,`products`.`price`,`products`.`real_price`,`products`.`description`,`products`.`country_id`,`products`.`ord`, `country`.`code3l` as cntName, `country`.`flag_128` as flag_128,`country`.`id` as cntId,' +
            ' `categories`.`name` as cat_name,`categories`.`name_pharsi` as cat_name_pharsi,`categories`.`logo` as cat_logo,`categories`.`color` as cat_color' +
            ' FROM `products`' +
            ' JOIN `country` ON `products`.`country_id` = `country`.`id`'+
            ' JOIN `categories` ON `categories`.`id` = `products`.`cat_id`'+
            ' WHERE `products`.`cat_id`=' + db.escape(cat_id);
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getCatalog",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getCatalog"))
            }else{
                for(var i = 0; i < result.length; i++){
                    result[i].img = static.MAIN_URL + '/images/flags/'+result[i].flag_128
                    result[i].cat_imgs = static.MAIN_URL + '/images/logos/'+result[i].cat_logo
                }
                resolve(result)
            }
        })
    })
};

var getGuestBankNumbers = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `bank_number_not_verifyed`.`bank_card_number` ' +
            'FROM `bank_number_not_verifyed` ' +
            'WHERE `bank_number_not_verifyed`.`id` = "1"';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getGuestBankNumbers",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getGuestBankNumbers"))
            }else{
                resolve(result)
            }
        })
    })
};
var findBankNumberForUser = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            'SELECT `bank_number_verifyed`.`bank_card_number`,`bank_number_verifyed`.`id` ' +
            'FROM `bank_number_user_show` ' +
            'LEFT JOIN `bank_number_verifyed` ON  `bank_number_verifyed`.`id` = `bank_number_user_show`.`bank_number_verifyed_id` ' +
            'WHERE `bank_number_user_show`.`user_id` = "'+user_id+'"';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findBankNumberForUser",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getGuestBankNumbers"))
            }else{
                resolve(result)
            }
        })
    })
};

module.exports.getFlipMoneys = getFlipMoneys;
module.exports.getCatalog = getCatalog;
module.exports.getGuestBankNumbers = getGuestBankNumbers;
module.exports.getCatalogByCountries = getCatalogByCountries;
module.exports.findBankNumberForUser = findBankNumberForUser;
