const db = require('../connection');
var log = require("../../logs/log");
const static = require('../../static');

var findAllByFilds = function(table,filds,cb){
    var queryFild = "";
    if(filds == "*"){
        queryFild = filds;
    }else{
        queryFild = filds.join(' , ')
    }
    var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
    db.query(prepareSql, function (error, result, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findAllByFilds",
                    table:table,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    })
}

var findByIdMulty = function(table,filds,id,cb){
    var queryFild = "";
    var whereFild = "";
    if(filds == "*"){
        queryFild = filds;
    }else{
        queryFild = filds.join(' , ')
    }
    for(var i = 0; i < id.length; i++){
        if(i == id.length - 1){
            whereFild += '`id` = ' + id[i] + ' '
        }else{
            whereFild += '`id` = ' + id[i] + ' OR '
        }

    }
    // whereFild = id.join(' OR ')


    var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+' WHERE '+ whereFild +'';
    db.query(prepareSql, function (error, result, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findAllByFilds",
                    table:table,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    })
}



/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
    console.log('prepareSql',prepareSql)
    db.query(prepareSql, function (error, rows, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findByMultyName2v",
                    table:table,
                    params:params,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}

/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var insert2v = function(table,post,cb){
    var prepareSql = 'INSERT INTO '+table+' SET ?';
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var findAllByFildsPromise = function(table,filds){
    return new Promise(function (resolve, reject) {
        var queryFild = "";
        if(filds == "*"){
            queryFild = filds;
        }else{
            queryFild = filds.join(' , ')
        }
        var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllByFildsPromise"))
            }else{
                resolve(result)
            }
        })
    })
}


var findAllPromise = function(table){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT * FROM '+table+'';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllPromise"))
            }else{
                resolve(result)
            }
        })
    })
}


var findAllSlidePromise = function(){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `slide`.`product_id`,' +
            ' `products`.`cat_id`,`products`.`price`,`products`.`real_price`,`products`.`country_id`,`products`.`status_buy`, ' +
            ' `categories`.`name`,`categories`.`name_pharsi`,`categories`.`logo`,`categories`.`color` ' +
            ' FROM `slide`' +
            ' JOIN `products` ON `products`.`id` = `slide`.`product_id` AND `products`.`status_buy` = 0'+
            ' JOIN `categories` ON `categories`.`id` = `products`.`cat_id`';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllSlidePromise",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllSlidePromise"))
            }else{
                resolve(result)
            }
        })
    })
}
var findUserOrders = function(order_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `order_products`.`voucher_code`,' +
            ' `products`.`cat_id`,`products`.`price`,`products`.`real_price`, ' +
            ' `categories`.`name`,`categories`.`name_pharsi`,`categories`.`logo`,`categories`.`color` ' +
            ' FROM `order_products` ' +
            ' JOIN `products` ON `products`.`id` = `order_products`.`product_id` '+
            ' JOIN `categories` ON `categories`.`id` = `products`.`cat_id` ' +
            ' WHERE `order_products`.`order_id` = '+db.escape(order_id)+'';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllSlidePromise",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllSlidePromise"))
            }else{
                resolve(result)
            }
        })
    })
}


var findUserSaleOrders = function(user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `sale_orders`.`amount`,`sale_orders`.`data` as saleOrderData,' +
            ' `sale_orders`.`alfa`,`sale_orders`.`beta`,`sale_orders`.`status_api`,`sale_orders`.`status_admin`, ' +
            ' `categories`.`name`,`categories`.`logo`,`categories`.`color` ' +
            ' FROM `sale_orders` ' +
            ' JOIN `categories` ON `categories`.`id` = `sale_orders`.`cat_id` ' +
            ' WHERE `sale_orders`.`user_id` = '+db.escape(user_id)+'';
        db.query(prepareSql, function (error, result) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserSaleOrders",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findUserSaleOrders"))
            }else{
                resolve(result)
            }
        })
    })
}


var findByMultyNamePromise = function(table,params,filds){
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for(var key in params){
            if(params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            }else{
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMultyNamePromise",
                        table:table,
                        params:params,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                resolve(rows)
            }
        });
    })
}


var findByMultyNameForCartPromise = function(table,user_id){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `cart`.`product_id`, `cart`.`count` , `products`.`real_price` ' +
            ' FROM ' + table+
            ' JOIN `products` ON `products`.`id` = `cart`.`product_id`'+
            ' WHERE `cart`.`user_id` = '+user_id+'';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMultyNameForCartPromise",
                        params:params,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                resolve(rows)
            }
        });
    })
}


var getFlipMoneyItem = function(id,cb){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `flipmoney_cards`.`price`,`flipmoney_cards`.`id`, `flipmoney_rate`.`rate` ' +
            ' FROM `flipmoney_cards` '+
            ' JOIN `flipmoney_rate` '+
            ' WHERE `flipmoney_cards`.`id` = '+id;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMultyNameForCartPromise",
                        params:params,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error getFlipMoneyItem"))
            }else{
                resolve(rows)
            }
        });
    })
}


function insert2vPromise(table,post){
    return new Promise(function (resolve, reject) {
        // var key = "password";
        // delete post[key]
        var prepareSql = 'INSERT INTO '+table+' SET ?';
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insert2vPromise",
                        table:table,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

function insertPhoneCodePromise(post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO phone_code SET ?';
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertPhoneCodePromise",
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt");
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}


var insertLoopPromise = function(table,filds,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO ' + table + ' (' + filds.join(',') + ') VALUES  ?';
        var query = db.query(prepareSql, [post], function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertLoopPromise",
                        table:table,
                        filds:filds,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error insertLoopPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

function deletePromise(table,params) {
    return new Promise(function (resolve, reject) {
        var where = [];
        for (var i = 0; i < params.length; i++) {
            where.push("`" + params[i]['key'] + "`=" + db.escape(params[i]['value']));
        }
        var prepareSql = "DELETE FROM  " + table + " WHERE " + where.join(" OR ") + " ";
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletePromise",
                        table: table,
                        params: params,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
                reject(new Error("Sql error deletePromise"))
            } else {
                resolve({error: false})
            }
        });
    })
}


function findByToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `users_db` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByTokenMulty(table,token){
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT `id` FROM  " + table + " WHERE `token`=" + db.escape(token);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByProductId(selectField,selectid,productId){
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT * FROM  cart WHERE `"+selectField+"`=" + db.escape(selectid)+" AND `product_id` = "+  db.escape(productId);
            // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'cart',
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function findByGuestToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `guest` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findByGuestToken",
                        table: 'guest',
                        params: {token:token},
                        filds: "*",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error findByGuestToken"))
            } else {
                resolve(rows)
            }
        })
    })
}

function updatePromise(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for(var key in post.values){
            val.push("`"+key +"`="+db.escape(post.values[key]));
        }
        var whereVal = [];
        for(var key in post.where){
            whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "updatePromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                if(errorData.errno == '1062' && (errorData.devData.table == 'orders' || errorData.devData.table == 'compare' || errorData.devData.table == 'favorite' || errorData.devData.table == 'product_last_show')){
                    getAndDeleteDuplicateKeysFromOrder(errorData.devData.table,errorData.devData.params.values.user_id,errorData.devData.params.where.guest_id,function (errorInfo) {
                        if(!errorInfo.error){
                            updatePromise("orders",errorData.devData.params)
                                .then(function (results) {
                                    resolve(results)
                                })
                                .catch(function () {
                                    log.insertLog(JSON.stringify({funcName:"getAndDeleteDuplicateKeysFromOrder"}), "./logs/query2v.txt");
                                    reject(new Error("Sql error updatePromise 2"));
                                })
                        }else{
                            deleteMultyRowBy('orders',[{key:"guest_id",value:errorData.devData.params.where.guest_id}],function (errorInfo) {
                                resolve(results)
                            })
                        }
                    })
                }else {
                    log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
                    reject(new Error("Sql error updatePromise"));
                }
            }else{
                resolve(results)
            }
        });
    })
}

function getAndDeleteDuplicateKeysFromOrder(table,user_id,guest_id,cb) {
    var prepareSql = "SELECT `"+table+"`.`id`,`orders` .`product_item_id`,COUNT(`"+table+"` .`product_item_id`) as cc FROM `"+table+"` "+
        "WHERE `"+table+"`.`user_id` = "+db.escape(user_id)+" OR `orders`.`guest_id` = "+db.escape(guest_id)+" ";
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "getEndDeleteDuplicateKeysFromOrder",
                    table: table,
                    params: {user_id:user_id,guest_id:guest_id},
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
            cb({error:true})
        }else{
            var orderId = [];
            if(results.length>0) {
                for (var i = 0; i < results.length; i++) {
                    if (results[i]['cc'] > 1) {
                        orderId.push(
                            {
                                key:"id",
                                value:results[i]['id']
                            }
                        )
                    }
                }

                deleteMultyRowBy(table,orderId,function (errorInfo) {
                    cb(errorInfo)
                })
            }else{
                cb({error:true})
            }
        }
    })
}

function deleteMultyRowBy(table,params,cb) {
    var where = [];
    for(var i=0;i<params.length;i++){
        where.push("`"+params[i]['key'] +"`="+db.escape(params[i]['value']));
    }
    var prepareSql = "DELETE FROM  "+table+" WHERE "+where.join(" OR ")+" ";
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deleteMultyRowBy",
                    table: table,
                    params: params,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
            cb({error:true})
        }else{
            cb({error:false})
        }
    });
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2v",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var updateMultiple = function(table,post,cb){

    var prepareSql = [];

    for(var i = 0; i < post.length; i++){
        prepareSql.push("UPDATE  `products`  SET  `status_buy` = "+db.escape(post[i].set.status_buy)+" "+
            " WHERE `id`  =  "+ db.escape(post[i].where.id) +"");
    }
    var query = db.query(prepareSql.join(";"), function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2v",
                    table: table,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}
var updateMultipleOrderProducts = function(post,cb){

    var prepareSql = [];

    for(var i = 0; i < post.length; i++){
        // console.log('aaaa',post)
        // console.log('bbbb',post[i])
        // console.log('wwww',post[i].codes)
        var jsonBody = JSON.parse(post[i].codes);
        // console.log('cccc',jsonBody)
        // console.log('jsonBody.data["a_code"]',jsonBody.data['a_code'])
        // console.log('jsonBody.data["b_code"]',jsonBody.data['b_code'])
        prepareSql.push("UPDATE  `order_products`  SET  `voucher_code` =  'alfa: "+jsonBody.data['a_code']+"    beta: "+ jsonBody.data['b_code'] +"'"+
            " WHERE `id`  =  "+ db.escape(post[i].id) +"");
    }
    // console.log('prepareSql',prepareSql)
    var query = db.query(prepareSql.join(";"), function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "updateMultipleOrderProducts",
                    table: `order_products`,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    db.query(prepareSql,post,function(error,result,fields){
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deletes",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    });
}

var deletesPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post) {
            if (post[key] !== "NULL") {
                val.push("`" + key + "`=" + db.escape(post[key]));
            } else {
                val.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'DELETE FROM ' + table + ' WHERE ' + val.join(' AND ') + '';
        db.query(prepareSql, post, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletesPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}


function findByEmail(email){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id`,`email`,`f_name`,`l_name`,`m_name` FROM  `users_db` WHERE `email`=' + db.escape(email);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmail",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByEmailAndCodeForForgotPassword(queryParams){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `forgot_password_key` WHERE `user_id`=' + db.escape(queryParams.user_id)+' AND `ver_code`='+db.escape(queryParams.code);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmailAndCodeForForgotPassword",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function findByCodeId(codeId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `user_id` FROM  `forgot_password_key` WHERE `id`=' + db.escape(codeId);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByCodeId",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}


function findProductsByCategories(catids){

    return new Promise(function (resolve, reject) {
        var where = [];
        for(var i=0;i<catids.length;i++){
            where.push("`categories`.`id`= "+db.escape(catids[i]));
        }
        var prepareSql = 'SELECT `categories`.*, ' +
            '`products`.`id` as prod_id, `products`.`cat_id`, `products`.`price`, `products`.`real_price`,`products`.`description` ' +
            ' FROM  `categories` ' +
            ' JOIN `products` ON `products`.`cat_id` = `categories`.`id`' +
            ' WHERE '+where.join(' OR' )+' GROUP BY `products`.`id` ';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByCodeId",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        })
    })
}

function findProductsById(prodId){

    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `products`.`id` as prod_id, `products`.`cat_id`, `products`.`price`, `products`.`real_price`,`products`.`description`, ' +
            ' `categories`.*' +
            ' FROM  `products` ' +
            ' JOIN `categories` ON `products`.`cat_id` = `categories`.`id` ' +
            ' WHERE `products`.`id` = '+prodId;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByCodeId",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        })
    })
}



function findOrderForUser(user_id){

    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `orders`.`user_id`,`orders`.`cretae_time` as orderCreateTime,`orders`.`id` as orderId,`orders`.`status` as orderStatus,`orders`.`cart` as orderCart, ' +
            ' `order_products`.`product_id`,`order_products`.`voucher_code`, ' +
            ' `products`.`cat_id`,`products`.`price`,`products`.`real_price`, ' +
            ' `categories`.`name`,`categories`.`logo`,`categories`.`color` ' +
            ' FROM `orders`' +
            ' JOIN `order_products` ON `order_products`.`order_id` = `orders`.`id`' +
            ' JOIN `products` ON `products`.`id` = `order_products`.`product_id`' +
            ' JOIN `categories` ON `categories`.`id` = `products`.`cat_id`' +
            ' WHERE `orders`.`user_id` = '+ db.escape(user_id) +' AND `orders`.`ref_id` IS NOT NULL AND `orders`.`status` = "1" ' +
            ' ORDER BY `orders`.`id` DESC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByCodeId",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        })
    })
}
function findOrderProducts(orderId){

    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT  ' +
            ' `order_products`.`product_id`,`order_products`.`voucher_code`,`order_products`.`id` as orderProductId, ' +
            ' `products`.`real_price` ' +
            ' FROM `order_products`' +
            ' JOIN `products` ON `products`.`id` = `order_products`.`product_id`' +
            ' WHERE `order_products`.`order_id` = '+ db.escape(orderId)+'';
        console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findOrderProducts",
                        table:'order_products',
                    }
                }
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findAllC2c(user_id){

    return new Promise(function (resolve, reject) {
        var prepareSql = '' +
            ' SELECT `c2c`.`amount`,`c2c`.`request_date` as c2cReqDate,`c2c`.`id` as c2cId,`c2c`.`user_bank_card_number`,`c2c`.`request_date`, ' +
            ' `c2c`.`voucher_code`,`c2c`.`admin_status`, ' +
            ' `categories`.`name`,`categories`.`logo`,`categories`.`color` ' +
            ' FROM `c2c`' +
            ' JOIN `categories` ON `categories`.`id` = `c2c`.`cat_id`' +
            ' WHERE `c2c`.`user_id` = '+ db.escape(user_id) +
            ' ORDER BY `c2c`.`id` DESC';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllC2c",
                        table:'c2c',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                for(var i = 0; i < rows.length; i++){
                    rows[i].logo = static.MAIN_URL + "/images/logos/" + rows[i]['logo'];
                }
                resolve(rows)
            }
        })
    })
}


function findByMultyProductId(id,cb){
    var whereFild = "";
    for(var i = 0; i < id.length; i++){
        if(i == id.length - 1){
            whereFild += '(`id` = ' + id[i] + ' AND `cat_id` = "27") '
        }else{
            whereFild += '(`id` = ' + id[i] + ' AND `cat_id` = "27") OR '
        }

    }

    var prepareSql = 'SELECT * FROM `products` WHERE '+ whereFild +'';
    console.log('prepareSql',prepareSql)
    db.query(prepareSql, function (error, result, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findByMultyProductId",
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    })
}


module.exports.findByIdMulty = findByIdMulty;
module.exports.findByEmail = findByEmail;
module.exports.findAllSlidePromise = findAllSlidePromise;
module.exports.findByEmailAndCodeForForgotPassword = findByEmailAndCodeForForgotPassword;
module.exports.findByCodeId = findByCodeId;
module.exports.findProductsByCategories = findProductsByCategories;
module.exports.findProductsById = findProductsById;
module.exports.findByProductId = findByProductId;
module.exports.findByMultyNameForCartPromise = findByMultyNameForCartPromise;
module.exports.findAllByFilds = findAllByFilds;
module.exports.findAllByFildsPromise = findAllByFildsPromise;
module.exports.findAllPromise = findAllPromise;
module.exports.findByMultyName = findByMultyName;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
module.exports.insert2v = insert2v;
module.exports.update2v = update2v;
module.exports.updatePromise = updatePromise;
module.exports.deletes = deletes;
module.exports.deletesPromise = deletesPromise;
module.exports.insert2vPromise = insert2vPromise;
module.exports.insertLoopPromise = insertLoopPromise;
module.exports.deletePromise = deletePromise;
module.exports.findByToken = findByToken;
module.exports.findByGuestToken = findByGuestToken;
module.exports.findByTokenMulty = findByTokenMulty;
module.exports.insertPhoneCodePromise = insertPhoneCodePromise;
module.exports.updateMultiple = updateMultiple;
module.exports.findOrderForUser = findOrderForUser;
module.exports.findByMultyProductId = findByMultyProductId;
module.exports.getFlipMoneyItem = getFlipMoneyItem;
module.exports.findUserOrders = findUserOrders;
module.exports.findUserSaleOrders = findUserSaleOrders;
module.exports.findAllC2c = findAllC2c;
module.exports.findOrderProducts = findOrderProducts;
module.exports.updateMultipleOrderProducts = updateMultipleOrderProducts;
