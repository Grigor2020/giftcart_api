var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var querys = require('../../model/dbQuerys/querys');
var imgDownload = require('../../model/dbQuerys/image-download/downloadimg');
var statics = require('../../static');
var Kavenegar = require('kavenegar');
var request = require('request');
const db = require('../../model/connection');

router.post('/flipmoney/add',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var user = {
        user:null,
        user_id:req.user_id
    };
    switch (req.user_type) {
        case "user":
            user.user = "user_id";
            break;
        case "guest":
            user.user = "guest_id";
            break;
    }
    if( user.user == "guest_id"){
        res.json({error: true, msg: "999999999-999999"});
        res.end();
    }else{
        var vId = req.body.params.id;
        querys.getFlipMoneyItem(vId)
            .then(function (result) {
                for(var i = 0; i < result.length; i++) {
                    result[i].real_price = parseFloat(result[i].price) * parseFloat(result[i].rate);
                    delete result[i].rate
                }

                const formData = {
                    "apiKey" :"3a50832960164e10",
                    "amount" : result[0].real_price
                };
                request.post(
                    {
                        url: 'https://secure.flipmoney.net/vapp/code-gen/',
                        form: formData
                    },
                    function (err, httpResponse, body) {
                        var jsonBody = JSON.parse(body);


                        var insertDataProduct = {
                            cat_id:27,
                            price:result[0].price,
                            real_price:result[0].real_price,
                            description:'',
                            country_id:'252',
                            v_code:"alfa: "+jsonBody.data['a_code']+"    beta: "+ jsonBody.data['b_code'] +"",
                            status_buy:'0',
                            ord:'0',
                        };
                        console.log('insertDataProduct.v_code',insertDataProduct.v_code);
                        querys.insert2vPromise('products',insertDataProduct)
                            .then(function (resultInsertProduct) {
                                var insertDataCart = {
                                    user_id:user.user_id,
                                    guest_id:null,
                                    product_id : resultInsertProduct.insertId,
                                    count : 1,
                                };
                                querys.insert2vPromise('cart',insertDataCart)
                                    .then(function (resultInsertCart) {
                                        var insertDataOrders = {
                                            user_id:user.user_id,
                                            total:insertDataProduct.real_price,
                                            status:0,
                                            cart:null,
                                            ref_id:null
                                        };
                                        querys.insert2vPromise('orders',insertDataOrders)
                                            .then(function (resultInsertOrders) {
                                                var insertDataOrderProducts = {
                                                    order_id:resultInsertOrders.insertId,
                                                    product_id : resultInsertProduct.insertId,
                                                    count: 1
                                                };
                                                querys.insert2vPromise('order_products',insertDataOrderProducts)
                                                    .then(function (resultOrderProducts) {
                                                        res.json({error: false, msg: "ok",data:{total : insertDataProduct.real_price, insertId : resultInsertOrders.insertId}});
                                                        res.end();
                                                    })
                                                    .catch(function (error) {
                                                        console.log('errorOrderProducts',error);
                                                        res.json({error: true, msg: "1111111"});
                                                        res.end();
                                                    })
                                            })
                                            .catch(function (error) {
                                                console.log('errorInsertOrders',error)
                                                res.json({error: true, msg: "2222222"});
                                                res.end();
                                            })
                                    })
                                    .catch(function (error) {
                                        console.log('errorInsertCart',error)
                                        res.json({error: true, msg: "3333333"});
                                        res.end();
                                    })
                            })
                            .catch(function (error) {
                                console.log('errorDataProduct',error)
                                res.json({error: true, msg: "44444444"});
                                res.end();
                            })
                    }
                );
            })
            .catch(function (error) {
                console.log('errorgetFlipMoneyItem',error)
                res.json({error: true, msg: "55555"});
                res.end();
            })
    }
});

router.post('/all',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var params = {
        user:null,
        user_id:req.user_id
    };
    switch (req.user_type) {
        case "user":
            params.user = "user_id";
            break;
        case "guest":
            params.user = "guest_id";
            break;
    }
    if( params.user == "guest_id"){
        return false
    }else{

        querys.findOrderForUser(params.user_id)
            .then(function (orders) {
                querys.findByIdMulty('users_db','*',[params.user_id],function (error,user) {
                    var userThis = user[0];
                    querys.findByMultyName('user_passport',{user_id:user[0].id,status:2},['bank_card_number'], function (error,cards) {
                        if(!error) {
                            var orderResults = [];
                            var cardsResults = [];
                            for (var i = 0; i < cards.length; i++) {
                                var hashCart =
                                    cards[i].bank_card_number[0] +
                                    cards[i].bank_card_number[1] +
                                    cards[i].bank_card_number[2] +
                                    cards[i].bank_card_number[3] +
                                    cards[i].bank_card_number[4] +
                                    cards[i].bank_card_number[5] +
                                    '******' +
                                    cards[i].bank_card_number[12] +
                                    cards[i].bank_card_number[13] +
                                    cards[i].bank_card_number[14] +
                                    cards[i].bank_card_number[15];
                                cardsResults.push(hashCart);
                            }

                            for (var j = 0; j < orders.length; j++) {
                                if (cardsResults.indexOf(orders[j].orderCart) != -1) {
                                    orderResults.push(orders[j])
                                } else {
                                    orders[j].v_code = 'کارت شما تایید نشده . پس از تایید آن کد نمایش داده میشود';
                                    orderResults.push(orders[j])
                                }
                            }

                            res.json({error: false, msg: "ok", data: orderResults, cardsResults: cardsResults});
                            res.end();
                        }
                    })
                })

            })
            .catch(function (error) {
                res.json({error: true, msg: "894651561352"});
                res.end();
            })
    }
});

router.post('/create',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var params = {
        user:null,
        user_id:req.user_id
    };
    switch (req.user_type) {
        case "user":
            params.user = "user_id";
            break;
        case "guest":
            params.user = "guest_id";
            break;
    }
    if(params.user === "user_id"){
        querys.findByMultyNameForCartPromise('cart',req.user_id)
            .then(function (result) {

                var total = 0;
                for(var i = 0; i < result.length; i++){
                    total += parseFloat(result[i].real_price) * parseInt(result[i].count)
                }
                var insData = {
                    user_id : req.user_id,
                    status : 0,
                    total : total
                }
                querys.insert2vPromise('orders',insData)
                    .then(function (resultInsert) {
                        var fild = ['order_id','product_id','voucher_code'];
                        var post = [];
                        for(var i = 0; i < result.length; i++){
                            var thisProductCount = parseInt(result[i].count);
                            for(var j = 0; j < thisProductCount; j++){
                                post.push(
                                    [
                                        resultInsert.insertId,
                                        result[i].product_id,
                                        null,
                                        // i+''+j
                                    ]
                                )
                            }

                        }
                        var resProds = [];
                        for(var i = 0; i < result.length; i++){
                            var thisProductCount = parseInt(result[i].count);
                            for(var j = 0; j < thisProductCount; j++){
                                resProds.push(result[i].product_id)
                            }
                        }
                        querys.insertLoopPromise('order_products',fild,post)
                            .then(function (data) {
                                res.json({error: false, msg: "ok",data:{total : total,insertId : resultInsert.insertId}});
                            })
                            .catch(function () {
                                res.json({error: true, msg: "876156861568135"});
                                res.end();
                            })
                    })
                    .catch(function () {
                        res.json({error: true, msg: "786546864648654"});
                        res.end();
                    })
            })
            .catch(function () {
                res.json({error: true, msg: "023756854645"});
                res.end();
            })
    }else {
        res.json({error: true, msg: "141358483513513"});
        res.end();
    }

});


router.post('/success',function(req, res, next) {
    console.log('req.body',req.body)
    var updateParams = {
        values:{cart:req.body.cart,ref_id:req.body.ref_id,status:parseInt(req.body.status)},
        where:{id:req.body.order_id}
    };
    querys.findByMultyNamePromise('orders',{id:req.body.order_id},['user_id'])
        .then(function (userInfo) {
            if(req.body.status == '1'){
                querys.findByMultyNamePromise('users_db',{id:userInfo[0].user_id},['phone','id'])
                    .then(function (userPhone) {
                        var orderId = req.body.order_id;
                        var phoneNumber = userPhone[0].phone;
                        var userId = userPhone[0].id
                        querys.updatePromise('orders',updateParams)
                            .then(function () {
                                var orderId = req.body.order_id;
                                Promise.all([
                                    querys.findUserOrders(orderId),
                                    querys.findByMultyNamePromise('users_db',{id:userId},['phone'])
                                ])
                                    .then(function ([orders,user]){
                                        querys.findOrderProducts(orderId)
                                            .then(function (resultOrderProducts) {
                                                // console.log('......resultOrderProducts',resultOrderProducts)
                                                var updatePostArray = [];
                                                var createFlipArr = [];
                                                for(var i = 0; i < resultOrderProducts.length; i++){
                                                    createFlipArr.push(
                                                        {
                                                            id:resultOrderProducts[i].orderProductId,
                                                            refId : req.body.ref_id,
                                                            real_price:resultOrderProducts[i].real_price,
                                                            voucher_code:null
                                                        }
                                                        )
                                                }
                                                getFliipMOney(0,createFlipArr,function (result) {
                                                    querys.updateMultipleOrderProducts(result, function () {
                                                        sendOrderSms(0,result,phoneNumber,function () {
                                                            res.json({error: false});
                                                        })
                                                    })
                                                });

                                            })
                                            .catch(function () {
                                                res.json({error: true, msg: "02375685464782115"});
                                                res.end();
                                            })
                                    })
                                    .catch(function (){
                                        res.json({error: true, msg: "52686156441"});
                                        res.end();
                                    })
                            })
                            .catch(function () {
                                res.json({error: true, msg: "5646516846256"});
                                res.end();
                            })
                    })
                    .catch(function () {
                        res.json({error: true, msg: "7451568412321"});
                        res.end();
                    })
            }else{
                res.json({error: false});
            }
        })
        .catch(function () {
            res.json({error: true, msg: "15648964615"});
            res.end();
        })


});


function sendOrderSms(num,param,phone,cb) {
    if(num!=param.length){

        var api = Kavenegar.KavenegarApi({
            apikey: '725969786479384753727A6473334F6B4E636F534B5574746858724B3842332F'
        });
        // console.log('phone',phone)
        // console.log('param[num]',param[num])
        // console.log('param[num].voucher_code',param[num].voucher_code)
        api.VerifyLookup({
            receptor: phone, // phone number
            token: param[num].codes, // token
            template: "verify2p0ne"
        }, function (response, status) {
            // console.log('status', status)
            if (status != '200') {
                // res.json({error: true, msg: "1253848631516"});
            }
        });
        num++;
        sendOrderSms(num,param,phone,cb);

    }else{
        cb(true)
    }
}



function getFliipMOney(num,param,cb) {
    if(num!=param.length){
        // console.log("param[num]",param[num])
        // console.log("param[num]['refId']",param[num]['refId'])
        // console.log("typeof param[num]['refId']",typeof param[num]['refId'])
        // const formData = {
        //     "apiKey" :"3a50832960164e10",
        //     "amount" : param[num]['real_price'],
        //     "details" : {"ZarinpalReferenceId" : param[num]['refId']}
        // };
        // console.log('formData',formData)
        // request.post(
        //     {
        //         url: 'https://secure2.imoneysecure.com/vapp/code-gen/',
        //         form: formData
        //     },
        //     function (err, httpResponse, body) {
        //         param[num]['codes'] = body
        //         num++;
        //         getFliipMOney(num,param,cb);
        //     }
        // );

        var options = {
            'method': 'POST',
            'url': 'https://secure2.imoneysecure.com/vapp/code-gen/',
            'headers': {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"apiKey":"3a50832960164e10","amount":param[num]['real_price'],"details":{"ZarinpalReferenceId": param[num]['refId']}})

        };
        request(options, function (error, response,body) {
            if (error) throw new Error(error);
            // console.log('response',response.request);
            console.log('body',body);
            param[num]['codes'] = body
            num++;
            getFliipMOney(num,param,cb);
        });
    }else{
        cb(param)
    }
}


function updateFlipMoneyProducts(num,param,cb) {
    if(num!=param.length){
        var obj = JSON.parse(param[num].codes);
        // var thisCode = "a_code = "+obj.data['a_code'] + "| b_code =  " + obj.data['b_code'];
        var thisCode = "alfa: "+obj.data['a_code']+"    beta: "+ obj.data['b_code'] +"";

        var post = {
            where:{'id' : param[num].id},
            values:{'v_code':thisCode}
        }
        var val = [];
        for(var key in post.values){
            val.push("`"+key +"`="+db.escape(post.values[key]));
        }
        var whereVal = [];
        for(var key in post.where){
            whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE `products` SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    devData: {
                        queryFunction: "updateFlipMoneyProducts",
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                cb(true,errorData);
            }else{
                cb(false,results)
            }
        });
    }else{
        cb(true,'5465423165')
    }
}

module.exports = router;
