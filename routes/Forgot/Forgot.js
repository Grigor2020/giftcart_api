var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var querys = require('../../model/dbQuerys/querys');
var statics = require('../../static');
var Kavenegar = require('kavenegar');
var sha1 = require('sha1');


router.post('/phone',function(req, res, next) {
    var phone = req.body.phone;

    querys.findByMultyNamePromise('users_db', {'phone':phone},['id','phone'])
        .then(function (user) {
            if(user.length === 1){
                var verData = {
                    user_id : user[0].id,
                    code  :genPhoneVer()
                };

                querys.findByMultyNamePromise('phone_code', {'user_id':user[0].id},['id'])
                    .then(function (result){
                        // console.log('result',result)
                        if(result.length === 0){
                            querys.insertPhoneCodePromise(verData)
                                .then(function (dada){
                                    var api = Kavenegar.KavenegarApi({
                                        apikey: '725969786479384753727A6473334F6B4E636F534B5574746858724B3842332F'
                                    });
                                    api.VerifyLookup({
                                        receptor: user[0].phone, // phone number
                                        token: verData.code, // token
                                        template: "loginpasscodeone"
                                    }, function(response, status) {
                                        if(status == '200'){
                                            res.json({error: false, msg: "i-ok",data:user[0].phone});
                                            res.end();
                                        }else{
                                            res.json({error: true, msg: "15861256841253"});
                                            res.end();
                                        }
                                    });
                                })
                                .catch(function () {
                                    res.json({error: true, msg: "7845138315"});
                                    res.end();
                                })
                        }else{
                            var newCode = genPhoneVer();
                            var post = {
                                where: {
                                    user_id : user[0].id
                                },
                                values : {
                                    code : newCode
                                }
                            }
                            querys.updatePromise('phone_code',post)
                                .then(function () {
                                    var api = Kavenegar.KavenegarApi({
                                        apikey: '725969786479384753727A6473334F6B4E636F534B5574746858724B3842332F'
                                    });
                                    api.VerifyLookup({
                                        receptor: user[0].phone, // phone number
                                        token: newCode, // token
                                        template: "loginpasscodeone"
                                    }, function(response, status) {
                                        if(status == '200'){
                                            res.json({error: false, msg: "u-ok",data:user[0].phone});
                                            res.end();
                                        }else{
                                            res.json({error: true, msg: "15861256841253"});
                                            res.end();
                                        }
                                    });
                                })
                                .catch(function () {
                                    res.json({error: true, msg: '453565678645345',data:[]});
                                    res.end();
                                })
                        }
                    })
                    .catch(function (error) {
                        res.json({error: true, msg: '7864556846456',data:[]});
                        res.end();
                    })
            }else{
                res.json({error: true, msg: '51235848316',data:[]});
                res.end();
            }
        })
        .catch(function (error) {
            res.json({error: true, msg: "41563158961523"});
            res.end();
        })

});


router.post('/phone/code',function(req, res, next) {
    var code = req.body.code;
    // console.log('code',code)
    querys.findByMultyNamePromise('phone_code', {'code':code},['user_id'])
        .then(function (data) {
            // console.log('data',data)
            var user_id = data[0].user_id;
            querys.findByMultyNamePromise('users_db', {'id':user_id},['token'])
                .then(function (resultUser) {
                    if(resultUser.length === 1){
                        querys.deletesPromise('phone_code',{code:code})
                            .then(function (resCode) {
                                res.json({error: false, msg: "ok",data:resultUser[0].token});
                                res.end();
                            })
                            .catch(function (error) {
                                res.json({error: true, msg: "6843543564534543"});
                                res.end();
                            })
                    }else{
                        res.json({error: true, msg: "7865435435315"});
                        res.end();
                    }
                })
                .catch(function (error) {
                    res.json({error: true, msg: "21345654464564"});
                    res.end();
                })
        })
        .catch(function (error) {
            res.json({error: true, msg: "45612348648"});
            res.end();
        })
});


router.post('/password/new',function(req, res, next) {
    var pass = req.body.pass;
    var repass = req.body.repass;
    var toks = req.body.toks;
    console.log('toks,',toks)
    if(pass === repass){
        querys.findByMultyNamePromise('users_db', {'token':toks},['id','f_name','l_name','email','status','phone','token'])
            .then(function (data) {
                console.log('data',data)
                if(data.length === 1){
                    var params = {
                        where:{id : data[0].id},
                        values:{token : toks,password : sha1(pass)},
                    }
                    querys.updatePromise('users_db',params)
                        .then(function (result) {
                            res.json({error: false, msg: "ok",data:data});
                            res.end();
                        })
                        .catch(function (error) {
                            res.json({error: true, msg: "15648315613"});
                            res.end();
                        })
                }else{
                    res.json({error: true, msg: "15648315613"});
                    res.end();
                }
            })
            .catch(function (error) {
                res.json({error: true, msg: "45612348348"});
                res.end();
            })
    }else{
        res.json({error: true, msg: "not match"});
        res.end();
    }
});

module.exports = router;



function genPhoneVer() {
    var gen = Math.floor(1000 + Math.random() * 8990);
    return gen;
}
