var express = require('express');
var router = express.Router();
var querys = require('../../model/dbQuerys/querys');
var queryCart = require('../../model/dbQuerys/cart/cart');
var log = require("../../logs/log");
var sha1 = require('sha1');
var static = require('../../static');

router.post('/', function(req, res, next) {
    var guestTok = "";
    if(typeof req.body.guest != "undefined"){
        guestTok = req.body.guest;
    }

    var loginParams = {
        phone:req.body.phone,
        password:sha1(req.body.password),
    }

    var filds = ['id','f_name','l_name','email','status','phone','token'];
    Promise.all([querys.findByMultyNamePromise("users_db",loginParams,filds),querys.findByMultyNamePromise("guest",{token:guestTok},['id'])])
        .then(function ([user, guest]) {

            if (user.length == 1) {
                // console.log('user',user)
                // if(user[0]['status'] != 0){
                    var guestId = guest[0]['id'];
                    var updateParams = {
                        values:{user_id:user[0]['id'],guest_id:null},
                        where:{guest_id:guestId}
                    }

                    var guestUp = [
                        querys.updatePromise("cart",updateParams)
                        // querys.updatePromise("favorite",updateParams),
                        // querys.updatePromise("orders",updateParams),
                        // querys.updatePromise("product_last_show",updateParams),
                    ];
                    Promise.all(guestUp)
                        .then(function (result) {
                            var userInfo = [
                                queryCart.getCartByUser({user:"user_id",user_id:user[0]['id']}),
                            ]
                            Promise.all(userInfo)
                                .then(function ([cart]) {
                                    // console.log('cart',cart)
                                    delete user[0]['id'];
                                    res.json({error: false, msg: "ok", data: {
                                            user:user,
                                            cart:cart
                                        }});
                                    res.end();
                                })
                                .catch(function (error) {
                                    // console.log("9999",error)
                                    log.insertLog(error,"./logs/v2/promiseError.txt")
                                    res.json({error: true, msg: "9999",text:error, data: null});
                                    res.end();
                                })
                        })
                        .catch(function (error) {
                            // console.log("9999",error)
                            log.insertLog(error,"./logs/v2/promiseError.txt")
                            res.json({error: true, msg: "9999",text:error, data: null});
                            res.end();
                        })
                // }else{
                //     res.json({error: true, msg: "461sda1d65f1sd651"});
                //     res.end();
                // }
            }else{
                res.json({error: true, msg: "104", data: null});
                res.end();
            }
        })
        .catch(function (error) {
            // console.log("9999-1",error)
            log.insertLog(error,"./logs/v2/promiseError.txt")
            res.json({error: true, msg: "9999-1",text:error, data: null});
            res.end();
        })

})

module.exports = router;
