var express = require('express');
var router = express.Router();
var query = require('../../model/dbQuerys/querys');
var sha1 = require('sha1');

router.post('/', function(req, res, next) {
    // if(typeof req.body.guest != 'undefined'){
    //
    // }else{
    //     res.json({error:true,msg:"parametr missing",data:null});
    //     res.end()
    // }
    var dateObj = new Date();
    setGuestToken(sha1(dateObj.getTime()),function (token) {
        res.json({error:false,msg:"ok",data:token});
        res.end()
    })
});

router.post('/check', function(req, res, next) {
    var guestToken = req.body.guest.replace(/ /g,'');
    query.findByMultyName("guest",{token:guestToken},['id'],function (error,data) {
        if(data.length==1){
            res.json({error:false,msg:"ok",data:[]});
            res.end()
        }else{
            var dateObj = new Date();
            setGuestToken(sha1(dateObj.getTime()),function (token) {
                // console.log('token',token)
                res.json({error:false,msg:"new-ok",data:token});
                res.end()
            })
        }
    })
});


module.exports = router;

function setGuestToken(guestToken,cb) {
    query.findByMultyName("guest",{token:guestToken},['id'],function (data) {
        var dateObj = new Date();
        if(data.length>0){
            var genNewToken = sha1(dateObj.getTime());
            setGuestToken(genNewToken,cb);
        }else{
            query.insert2v('guest',{token:guestToken},function (error,data) {
                // console.log('data',data)
                cb({id:data.insertId,token:guestToken})
            });
        }
    })
}
