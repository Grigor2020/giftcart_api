var express = require('express');
var router = express.Router();
var checkRequestUser = require('../../checkRequestUser');
var querys = require('../../../model/dbQuerys/querys');
var querysCatalog = require('../../../model/dbQuerys/catalog/catalog');



router.post('/flipmoney/all',function(req, res, next) {
    querysCatalog.getFlipMoneys()
        .then(function (result,prepareSql) {
            for(var i = 0; i < result.length; i++){
                result[i].real_price = parseFloat(result[i].price) * parseFloat(result[i].rate);
                delete result[i].rate
            }
            if(!result.error) {
                res.json({error: false, msg: "ok",data:result});
                res.end();
            }else{
                res.json({error: true, msg: "51773"});
                res.end();
            }
        })
        .catch(function (error) {
            res.json({error: true, msg: "84615616"});
            res.end();
        })
});



router.post('/',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    console.log('req.body.cat_id',req.body.cat_id)
    console.log('req.user_type',req.user_type)

    if(req.body.cat_id == '29') {
        querys.findByMultyNamePromise('flipmoney_rate',{id:1},['rate'])
            .then(function (rate) {
                if( req.user_type == "guest"){
                    querysCatalog.getGuestBankNumbers()
                        .then(function (result) {
                            res.json({error: false, msg: "ok", data: result});
                            res.end();
                        })
                        .catch(function (error) {
                            res.json({error: true, msg: "651556135156615"});
                            res.end();
                        })
                }else{
                    querysCatalog.findBankNumberForUser(req.user_id)
                        .then(function (data) {
                            console.log('data',data)
                            res.json({error: false, msg: "ok", data: data,flipRate : rate[0].rate});
                            res.end();
                        })
                        .catch(function (error) {
                            res.json({error: true, msg: "9842268415635"});
                            res.end();
                        })
                }
            })
            .catch(function (error) {
                res.json({error: true, msg: "899465169815"});
                res.end();
            })

    }else{
        querysCatalog.getCatalog(req.body.cat_id)
            .then(function (result) {
                if (!result.error) {
                    res.json({error: false, msg: "ok", data: result});
                    res.end();
                } else {
                    res.json({error: true, msg: "213154"});
                    res.end();
                }
            })
            .catch(function (error) {
                res.json({error: true, msg: "15384866531531"});
                res.end();
            })
    }
});

router.post('/current',function(req, res, next) {
    console.log('req.body',req.body)
    querysCatalog.getCatalogByCountries(req.body.cat_id,req.body.country_id)
        .then(function (result) {
            if(!result.error) {
                res.json({error: false, msg: "ok",data:result});
                res.end();
            }else{
                res.json({error: true, msg: "213154"});
                res.end();
            }
        })
        .catch(function (error) {
            res.json({error: true, msg: "1251454351616531"});
            res.end();
        })
});

module.exports = router;
