var express = require('express');
var router = express.Router();
var querys = require('../../../model/dbQuerys/querys');
var static = require('../../../static');


router.post('/all',function(req, res, next) {
    querys.findAllSlidePromise()
        .then(function (data) {
            for(var i = 0; i < data.length; i++){
                data[i].image = static.MAIN_URL + "/images/logos/" + data[i]['logo'];
            }
            res.json({error: false, msg: "ok",data:data});
            res.end();
        })
        .catch(function (error) {
            res.json({error: true, msg: "4785631865"});
            res.end();
        })
});

module.exports = router;