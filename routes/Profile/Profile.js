var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var querys = require('../../model/dbQuerys/querys');
var imgDownload = require('../../model/dbQuerys/image-download/downloadimg');
var statics = require('../../static');
var path = require('path');

router.post('/user/addpassport',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var insertParams = {
        bank_card_number : req.body.bankCardNumber,
        image : req.body.image,
        user_id : req.user_id
    };
    querys.insert2vPromise('user_passport',insertParams)
        .then(function (data) {
            res.json({error: false});
            res.end();
        })
        .catch(function () {
            res.json({error: true, msg: "786315764534834"});
            res.end();
        })
});


router.get('/user/passport/all',checkRequestUser.checkUserOrGuest,function(req, res, next) {

    querys.findByMultyNamePromise('user_passport',{'user_id':req.user_id},['bank_card_number','image','status','msg'])
        .then(function (result) {
            for(var i = 0; i < result.length; i++){
                result[i].image = statics.FRONT_URL + '/images/passports/'+result[i].image
            }
            res.json({error: false, msg: "ok",data:result});
            res.end();
        })
        .catch(function () {
            res.json({error: true, msg: "48641516813515135"});
            res.end();
        })
});


module.exports = router;
