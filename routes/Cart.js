var express = require('express');
var router = express.Router();
var checkRequestUser = require('./checkRequestUser');
var querys = require('../model/dbQuerys/querys');
var cartQuerys = require('../model/dbQuerys/cart/cart');

router.post('/add',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var insertParams = {
        count: req.body.count,
        product_id: req.body.prodId,
        user_id:null,
        guest_id:null,
    };

    var table = '';
    var token = '';
    if(typeof req.headers.void == 'undefined'){
        if(typeof req.headers.guest != 'undefined'){
            table = 'guest';
            token = req.headers.guest;
        }
    }else{
        table = 'users_db';
        token = req.headers.void;
    }

    querys.findByTokenMulty(table,token)
        .then(function (result) {
            let selectField;
            let selectId = result[0].id;
            let productId =req.body.prodId;
            if(table == 'guest'){
                insertParams.guest_id = result[0].id;
                selectField = 'guest_id';

            }else{
                insertParams.user_id = result[0].id;
                selectField = 'user_id';
            }
            // console.log('insertParams',insertParams);
            // return false
            querys.findByProductId(selectField,selectId,productId)
                .then(function (data) {
                    if(data.length > 0){
                        let updateParams = {
                            where: {
                                id: data[0].id
                            },
                            values: {
                                count : parseInt(req.body.count) + parseInt(data[0].count)
                            }
                        };
                        querys.updatePromise('cart',updateParams)
                            .then(function () {
                                res.json({error: false, msg: "ok"});
                                res.end();
                            })
                            .catch(function () {
                                res.json({error: true, msg: "2135468513"});
                                res.end();
                            })
                    }else{
                        cartQuerys.addToCart(insertParams)
                            .then(function (result) {
                                console.log('result',result)
                                var params = {
                                    user:selectField,
                                    user_id:selectId,
                                    id : result.insertId
                                };
                                cartQuerys.getCartByUserAfterInsert(params)
                                    .then(function (result) {
                                        res.json({error:false,msg:"1000",data:
                                                {
                                                    cart:result
                                                }
                                        });
                                        res.end();
                                    })
                                    .catch(function (error) {
                                        res.json({error:true,msg:"9999",text:"cart/add",data:null});
                                        res.end();
                                    })

                            })
                            .catch(function () {
                                res.json({error: true, msg: "2135468513"});
                                res.end();
                            })
                    }
                })
                .catch(function (error) {
                    res.json({error: true, msg: "2135468513"});
                    res.end();
                })

        })
        .catch(function () {
            res.json({error: true, msg: "7845168315135"});
            res.end();
        })
});
router.post('/all',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var params = {
        user:null,
        user_id:req.user_id
    };
    switch (req.user_type) {
        case "user":
            params.user = "user_id";
            break;
        case "guest":
            params.user = "guest_id";
            break;
    }
    cartQuerys.getCartByUser(params)
        .then(function (result) {
            res.json({error:false,msg:"1000",data:
                    {
                        cart:result
                    }
            });
            res.end();
        })
        .catch(function (error) {
            res.json({error:true,msg:"9999",text:"cart/all",data:null});
            res.end();
        })
});


router.post('/count',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    console.log('req.body',req.body)
    var updateParams = {
        product_id: req.body.prodId,
        actionType: req.body.actionType,
        user:null,
        user_id:req.user_id
    };

    switch (req.user_type) {
        case "user":
            updateParams.user = "user_id";
            break;
        case "guest":
            updateParams.user = "guest_id";
            break;
    }

    cartQuerys.updateCartCount(updateParams)
        .then(function () {
            res.json({error:false,msg:"1000"});
            res.end();
        })
        .catch(function () {
            res.json({error:true,msg:"9999"});
            res.end();
        })

});




router.post('/remove',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    console.log('req.body',req.body)
    var updateParams = {
        product_id: req.body.prodId,
        user:null,
        user_id:req.user_id
    };

    switch (req.user_type) {
        case "user":
            updateParams.user = "user_id";
            break;
        case "guest":
            updateParams.user = "guest_id";
            break;
    }

    cartQuerys.deleteCart(updateParams)
        .then(function () {
            res.json({error:false,msg:"1000"});
            res.end();
        })
        .catch(function () {
            res.json({error:true,msg:"9999"});
            res.end();
        })

});



module.exports = router;
