var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var querys = require('../../model/dbQuerys/querys');
var statics = require('../../static');
var Kavenegar = require('kavenegar');
var sha1 = require('sha1');
var querySignup = require('../../model/dbQuerys/auth/signup');
var request = require('request');
var log = require("../../logs/log");

router.post('/add',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var cat_id = req.body.cat_id;
    var amount = req.body.amount;
    var bank_card_number = req.body.bank_card_number;
    var user_bank_card_number = req.body.c2cUserBankCardNumber;

    querys.findByMultyNamePromise('c2c',{user_id : req.user_id,admin_status:'0'},['id'])
        .then(function (unviedRequest) {
            if(unviedRequest.length > 0){
                res.json({error: true, msg: "84651258961655336"});
                res.end();
            }else{
                if(cat_id == '27' || cat_id == '29') {


                    const formData = {
                        "apiKey": "3a50832960164e10",
                        "amount": amount
                    };

                    request.post(
                        {
                            url: 'https://secure2.imoneysecure.com/vapp/code-gen/',
                            form: formData
                        },
                        function (err, httpResponse, body) {

                            var codes = JSON.parse(body);
                            var insertPost = {
                                cat_id: cat_id,
                                amount: amount,
                                user_id: req.user_id,
                                bank_number: bank_card_number,
                                user_bank_card_number: user_bank_card_number,
                                voucher_code: "alfa: " + codes.data.a_code + "    beta: " + codes.data.b_code + ""
                            };
                            querys.insert2vPromise('c2c', insertPost)
                                .then(function (data) {
                                    res.json({error: false, msg: "ok"});
                                })
                                .catch(function (error) {
                                    res.json({error: true, msg: "45612348648"});
                                    res.end();
                                })
                        }
                    );
                }else{
                    var insertPost = {
                        cat_id: cat_id,
                        amount: amount,
                        user_id: req.user_id,
                        bank_number: bank_card_number,
                        user_bank_card_number: user_bank_card_number,
                    };
                    querys.insert2vPromise('c2c', insertPost)
                        .then(function (data) {
                            // console.log('data',data)
                            res.json({error: false, msg: "ok"});
                        })
                        .catch(function (error) {
                            res.json({error: true, msg: "45612348649"});
                            res.end();
                        })
                }
            }
        })
        .catch(function (error) {
            res.json({error: true, msg: "846512585153555336"});
            res.end();
        })
});


router.post('/all',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    querys.findAllC2c(req.user_id)
    // querys.findByMultyNamePromise('c2c', {user_id : req.user_id},['amount','user_bank_card_number','request_date','voucher_code','admin_status','id'])
        .then(function (data) {
            // console.log('data',data)
            res.json({error: false, data: data});
        })
        .catch(function (error) {
            res.json({error: true, msg: "87415318946156"});
            res.end();
        })
});


router.post('/phone-verification',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    var phone = req.body.phone;
    var sendingCode = genPhoneVer();
    var password = genPassword();
    querySignup.checkExsitUser({
        phone: phone,
    })
        .then(function (checkData) {
            if (checkData.length == 0) {
                sendSms(phone,sendingCode,function (response, status) {
                    console.log('status',status)
                    if(status == '200'){
                        var dateObj = new Date();
                        var insertUser = {
                            phone: req.body.phone,
                            password: sha1(password),
                            password_real: ""+password,
                            token: sha1(sha1(req.body.phone) + "" + dateObj.getTime()),
                            status: 0,
                        }
                        querys.insert2vPromise('users_db', insertUser)
                            .then(function (data) {
                                querys.findByMultyNamePromise('bank_number_verifyed',{defaults:'1'},['id','bank_card_number'])
                                    .then(function (bankCard){
                                        let insertUserShowData = {
                                            bank_number_verifyed_id : bankCard[0].id,
                                            user_id : data.insertId
                                        }
                                        querys.insert2vPromise('bank_number_user_show', insertUserShowData)
                                            .then(function (insertUserShowRes) {
                                                querys.insert2vPromise('phone_code', {code:sendingCode,user_id:data.insertId})
                                                    .then(function (data) {
                                                        // console.log('data',data)
                                                        sendPasswordSms(phone,password,function (response, status) {
                                                            if(status == '200'){
                                                                let user = {
                                                                    phone: insertUser.phone,
                                                                    token:insertUser.token,
                                                                    status:insertUser.status
                                                                };
                                                                res.json({error: false, msg: "ok",data:user});
                                                                res.end();
                                                            }else{
                                                                log.insertLog('Error code from cavenegar is - '+JSON.stringify(status),"./logs/v2/sms.txt")
                                                                res.json({error: true, msg: "123478965165"});
                                                                res.end();
                                                            }
                                                        });
                                                    })
                                                    .catch(function (error) {
                                                        res.json({error: true, msg: "45686516535153"});
                                                        res.end();
                                                    })
                                            })
                                            .catch(function () {
                                                res.json({error: true, msg: "9846512356894653"});
                                                res.end();
                                            })
                                    })
                                    .catch(function () {
                                        res.json({error: true, msg: "87456156415"});
                                        res.end();
                                    })
                            })
                            .catch(function (error) {
                                res.json({error: true, msg: "845132686415"});
                                res.end();
                            })
                    }else{
                        log.insertLog('Error code from cavenegar is - '+JSON.stringify(status),"./logs/v2/sms.txt")
                        res.json({error: true, msg: "12345989565"});
                        res.end();
                    }
                })
            }else{
                console.log('checkData',checkData)
                if(checkData[0].status == '0'){
                    let user = {
                        f_name:checkData[0].f_name,
                        l_name:checkData[0].l_name,
                        phone: checkData[0].phone,
                        token:checkData[0].token,
                        status:checkData[0].status
                    }
                    res.json({error: false, msg: "30001", data:user});
                    res.end();
                }else{
                    res.json({error: false, msg: "199", msgText: "User exsist"});
                    res.end();
                }
            }
        })
        .catch(function (error) {

        })

});



module.exports = router;



function genPhoneVer() {
    var gen = Math.floor(1000 + Math.random() * 8990);
    return gen;
}
function genPassword() {
    var gen = Math.floor(10000000 + Math.random() * 89999999);
    return ""+gen;
}

function sendSms(phone,sendingCode,cb){
    var api = Kavenegar.KavenegarApi({
        apikey: '725969786479384753727A6473334F6B4E636F534B5574746858724B3842332F'
    });
    api.VerifyLookup({
        receptor: phone, // phone number
        token: sendingCode, // token
        template: "verifyone"
    }, function(response, status) {
        cb(response, status)
    });
}
function sendPasswordSms(phone,sendingCode,cb){
    var api = Kavenegar.KavenegarApi({
        apikey: '725969786479384753727A6473334F6B4E636F534B5574746858724B3842332F'
    });
    api.VerifyLookup({
        receptor: phone, // phone number
        token: sendingCode, // token
        template: "verifyone"
    }, function(response, status) {
        cb(response, status)
    });
}
function sendOrderSms(phone,token1,token2,cb){
    var api = Kavenegar.KavenegarApi({
        apikey: '725969786479384753727A6473334F6B4E636F534B5574746858724B3842332F'
    });
    api.VerifyLookup({
        receptor: phone, // phone number
        token: token1, // token
        token2: token2, // token
        template: "Verify2P"
    }, function(response, status) {
        cb(response, status)
    });
}
