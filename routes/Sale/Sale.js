var express = require('express');
var router = express.Router();
var checkRequestUser = require('../checkRequestUser');
var querys = require('../../model/dbQuerys/querys');
var statics = require('../../static');
var request = require('request');

router.post('/add-code',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    if(typeof req.body.categorie !== "undefined") {
        if (req.body.categorie == '27') {
            if (typeof req.body.categorie !== "undefined" && typeof req.body.alfa !== "undefined" && typeof req.body.beta !== "undefined") {
                var categorie = req.body.categorie;
                var alfa = req.body.alfa;
                var beta = req.body.beta;
                // stex api call a gnum
                const formData = {
                    "a_code": req.body.alfa,
                    "b_code": req.body.beta
                };
                request.post(
                    {
                        url: 'https://secure2.imoneysecure.com/vapp/expire/',
                        form: formData
                    },
                    function (err, httpResponse, body) {
                        var jsonBody = JSON.parse(body);
                        console.log('jsonBody', jsonBody)
                        if (!jsonBody.expired) {
                            res.json({error: true, msg: "8456268453165156"});
                        } else {
                            var insertData = {
                                user_id: req.user_id,
                                cat_id: req.body.categorie,
                                amount: jsonBody.amount,
                                alfa: req.body.alfa,
                                beta: req.body.beta,
                                status_api: 1,
                                status_admin: 1,
                            }
                            console.log('insertData', insertData)
                            querys.insert2vPromise('sale_orders', insertData)
                                .then(function (data) {

                                })
                                .catch(function (error) {
                                    res.json({error: true, msg: "89423066536581"})
                                })
                        }
                    }
                );
            } else {
                res.json({error: true, msg: "8456268453165156"});
            }
        } else if (req.body.categorie == '28') {
            if (
                typeof req.body.categorie !== "undefined" &&
                typeof req.body.bank_num_1 !== "undefined" &&
                typeof req.body.bank_num_2 !== "undefined" &&
                typeof req.body.vaucherCode !== "undefined" &&
                typeof req.body.activationCode !== "undefined"
            ) {
                let categorie = req.body.categorie;
                let bank_num_1 = req.body.bank_num_1;
                let bank_num_2 = req.body.bank_num_2;
                let vaucherCode = req.body.vaucherCode;
                let activationCode = req.body.activationCode;

                // imitation that PM API says that code is valid
                let validCode = true
                if(validCode){
                    var insertData = {
                        user_id: req.user_id,
                        cat_id: categorie,
                        amount: 77888,
                        banc_card_1:bank_num_1,
                        banc_card_2: bank_num_2,
                        voucher_code: vaucherCode,
                        activation_code: activationCode,
                        status_api: 1,
                        status_admin: 1,
                    }
                    console.log('insertData', insertData)
                    querys.insert2vPromise('pm_sale_orders', insertData)
                        .then(function (data) {
                            res.json({error: false})
                        })
                        .catch(function (error) {
                            res.json({error: true, msg: "89423066536581"})
                        })
                }else{
                    res.json({error: true, msg: "8456268453165156"});
                }

            }
        } else {
            res.json({error: true, msg: "8941358654135"})
        }
    }
});


router.post('/all',checkRequestUser.checkUserOrGuest,function(req, res, next) {
    querys.findUserSaleOrders(req.user_id)
        .then(function (data) {
            res.json({error:false,data:data});
        })
        .catch(function (error) {
            res.json({error:true,msg:"21368496525684653"});
        })
});



module.exports = router;
