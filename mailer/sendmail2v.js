'use strict';
const nodemailer = require('nodemailer');
var fs = require('fs');
var mustache   = require('mustache');
var log = require("../logs/log");

function sendMailAuth(user_mail,backUrl,params,type) {

    nodemailer.createTestAccount(function (err, account) {
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'relay.lcnu.net',
            port: 25,
            secure: false, // true for 465, false for other ports
            auth: {
                user: "info@7shopday.com", // generated ethereal user
                pass: "iDX4mtQKvV" // generated ethereal password
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        var htmlMailPath = "";
        var htmlVars = {};
        var mailTitle = '';
        var view = {};
        if(type == 'phone_auth'){
            htmlMailPath = __dirname+"/auth_phone.html";
            htmlVars.user_name = params.user_name;
            htmlVars.phone_token = params.phone_token;
            mailTitle = "Подтверждение регистрации";
            view = {formatted: {user_name: htmlVars.user_name,phone_token: htmlVars.phone_token}}
        }else if(type == 'forgot_password'){
            htmlMailPath = __dirname+"/chnagepassword.html";
            htmlVars.user_name = params.user_name;
            htmlVars.ver_code = params.ver_code;
            mailTitle = "Verification Code";
            view = {formatted: {user_name: htmlVars.user_name,ver_code: htmlVars.ver_code}}
        }

        fs.readFile(htmlMailPath, 'utf8', function(err, content) {
            var output = mustache.render(content, view);
            let mailOptions = {
                from: '"7 km 👻" <info@7shopday.com>', // sender address
                to: '' + user_mail + ', ' + user_mail + '', // list of receivers
                subject: '' + mailTitle + ' ✔', // Subject line
                text: 'Hello user', // plain text body
                html: output
            };

            transporter.sendMail(mailOptions, function (error, info) {
                // console.log("error",error)
                // console.log("info",info)
                if(error != null) {
                    var errorData = {
                        dataView: view,
                        user_mail: user_mail,
                        params: params,
                        smtpError: error,
                        smtpInfo: info,
                    }
                    log.insertLog(JSON.stringify(errorData), "./logs/v2/sendmail.txt");
                }
                return 0;
            });
        })

    });
}

module.exports = sendMailAuth;