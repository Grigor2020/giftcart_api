var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var  catalogRouter = require('./routes/public/catalog/catalog');
var  categoriesRouter = require('./routes/public/categories/categories');
var  productRouter = require('./routes/public/product/product');
var  guestRouter = require('./routes/auth/guest');
var  signupRouter = require('./routes/auth/signup');
var  loginRouter = require('./routes/auth/login');
var  checkUserRouter = require('./routes/auth/checkUser');
var  cartRouter = require('./routes/Cart');
var  profileRouter = require('./routes/Profile/Profile');
var  ordersRouter = require('./routes/Orders/Orders');
var  contactRouter = require('./routes/ContactUs/ContactUs');
var  slideRouter = require('./routes/public/slide/slide');
var  forgotRouter = require('./routes/Forgot/Forgot');
var  saleRouter = require('./routes/Sale/Sale');
var  c2cRouter = require('./routes/c2c/c2c');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use(function (req, res, next) {
  // var ip = req.connection.remoteAddress;
  // console.log('ip',ip)
  res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization,guest");
  //res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
  //res.setHeader("Access-Control-Allow-Origin", "http://10.10.31.1");
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
  res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
  res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
  res.removeHeader('Server');
  res.removeHeader('X-Powered-By');
  next()
})

app.use(function (req, res, next) {
  if (req.headers['authorization'] == "753e925ebb3c2c1e20f06088249088613a321bd6") {
    // console.log(".........................")
    next();
  } else {
    res.json({error: true, msg: "1Authorization wrong", data: null});
    res.end();
  }
})

app.use('/api/v1/catalog', catalogRouter);
app.use('/api/v1/categories', categoriesRouter);
app.use('/api/v1/product', productRouter);
app.use('/api/v1/guest', guestRouter);
app.use('/api/v1/sign-up', signupRouter);
app.use('/api/v1/check-user', checkUserRouter);
app.use('/api/v1/sign-in', loginRouter);
app.use('/api/v1/cart', cartRouter);
app.use('/api/v1/profile', profileRouter);
app.use('/api/v1/orders', ordersRouter);
app.use('/api/v1/contact-us', contactRouter);
app.use('/api/v1/slide', slideRouter);
app.use('/api/v1/forgot', forgotRouter);
app.use('/api/v1/sale', saleRouter);
app.use('/api/v1/c2c', c2cRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
